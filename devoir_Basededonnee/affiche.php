<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link href="./bootstrap.min.css" rel="stylesheet">
  <script src="./bootstrap.bundle.min.js"></script>
  <title>Affiche</title>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body>
<h2 style="position: center;">Liste des athlethes</h2>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT numero, nom, universiter FROM mydb";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table class='table'><tr class='table-primary'><th>Numero</th><th>Nom</th><th>Universiter</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr class='table-secondary'><td>" . $row["numero"]. "</td><td>" . $row["nom"]. "</td><td>" . $row["universiter"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();
?>

</body>
</html>