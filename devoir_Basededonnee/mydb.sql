-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : sam. 16 juil. 2022 à 23:06
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mydb`
--

-- --------------------------------------------------------

--
-- Structure de la table `mydb`
--

CREATE TABLE `mydb` (
  `numero` int(20) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `universiter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `mydb`
--

INSERT INTO `mydb` (`numero`, `nom`, `universiter`) VALUES
(0, 'Doe', 'john@example.com'),
(0, 'Doe', 'john@example.com'),
(0, 'Doe', 'john@example.com'),
(0, 'Doe', 'john@example.com'),
(0, '', ''),
(0, 'Doe', 'john@example.com'),
(0, 'nom', 'univer'),
(0, 'nom', 'univer'),
(0, 'nom', 'univer'),
(0, 'nom', 'univer'),
(0, 'song', '0698682814');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
